@extends('layout.master')

@section('contant')

    <form action="{{ route('todo.update' , ['todo' => $todo->id]) }}" method="post" >
        @csrf
        @method('put')
        <div class="form-group">
            <label for="exampleInputEmail1">Edit TODO</label>
            <input name="to" value="{{ $todo['to'] }}" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Edit TODO">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Edit Description</label>
            <input name="do" value="{{ $todo['do'] }}" type="text" class="form-control" id="exampleInputPassword1" placeholder="Edit Description">
        </div>

        <button type="submit" class="btn btn-secondary">Edit</button>
    </form>

@endsection
