@extends('layout.master')

@section('contant')

    <form action="{{route('todo.store')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">TODO</label>
            <input validated name="to" type="text" class="form-control" id="exampleInputEmail1"  placeholder="TOdo">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Description</label>
            <input name="do" type="text" class="form-control" id="exampleInputPassword1" placeholder="Description">
        </div>

        <button type="submit" class="btn btn-success">Add</button>
    </form>

@endsection
