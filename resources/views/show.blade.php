@extends('layout.master')

@section('contant')
        @if($masseg = Session::get('msg'))
            <div class="alert alert-success">
                {{ $masseg }}
            </div>

        @endif


    @if($masseg = Session::get('msgdelet'))
        <div class="alert alert-success">
            {{ $masseg }}
        </div>

    @endif

        @if($masseg = Session::get('msgup'))
            <div class="alert alert-success">
                {{ $masseg }}
            </div>

        @endif

    @foreach($todos as $todo)

    <div class="card">
        <div class="card-header">
            ID : {{$todo['id']}}
        </div>
        <div class="card-body">
            <h3 class="card-title">{{$todo['to']}}</h3>
            <p> {{$todo['created_at']}}</p>
            <h5 class="card-text">{{$todo['do']}}</h5>
            <a href="{{ route('todo.edit' , $todo['id']) }}" class="btn btn-secondary">Edit</a>
            <form action="{{ route('todo.destroy' , ['todo' => $todo->id]) }}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger">Delet</button>
            </form>
        </div>
    </div>
        <br><br>
    @endforeach

@endsection
